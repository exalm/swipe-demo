[GtkTemplate (ui = "/org/example/SwipeDemo/row.ui")]
public class SwipeDemo.Row : Gtk.ListBoxRow {
    public signal void removed ();

    [GtkChild]
    private Gtk.Revealer revealer;

    [GtkCallback]
    private void remove_cb () {
        revealer.reveal_child = false;
    }

    [GtkCallback]
    private void notify_child_revealed_cb () {
        if (revealer.reveal_child || revealer.child_revealed)
            return;

        removed ();
    }

    public void reveal () {
        revealer.reveal_child = true;
    }
}

